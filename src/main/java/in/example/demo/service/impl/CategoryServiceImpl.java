package in.example.demo.service.impl;

import in.example.demo.entity.Category;
import in.example.demo.repo.CategoryRepository;
import in.example.demo.service.ICategoryService;
import java.lang.Long;
import java.lang.Override;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
@Service
public class CategoryServiceImpl implements ICategoryService {
  @Autowired
  private CategoryRepository repo;

  @Override
  @Transactional
  public Long saveCategory(Category category) {
    return repo.save(category).getId();
  }

  @Override
  @Transactional
  public void updateCategory(Category category) {
    repo.save(category);
  }

  @Override
  @Transactional
  public void deleteCategory(Long id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public Category getOneCategory(Long id) {
    return repo.findById(id).get();
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public List<Category> getAllCategorys() {
    return repo.findAll();
  }
}
