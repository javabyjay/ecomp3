package in.example.demo.repo;

import in.example.demo.entity.Category;
import java.lang.Long;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
